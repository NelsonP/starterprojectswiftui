# StarterProjectSwiftUI

![Version](https://img.shields.io/badge/version-0.0.1-blue.svg)
![Platforms](https://img.shields.io/badge/platform-iOS-blue.svg)
![Swift version](https://img.shields.io/badge/swift-5.0-blue.svg)

StarterProjectSwiftUI is a template project for SwiftUI applications following the MVVM architectural pattern. This project provides a starting point for new apps and includes some of the most commonly used features and best practices to ensure the long-term maintainability, scalability, and performance of your SwiftUI application.

## Features

- **Clean Architecture**: The project is built following the MVVM (Model-View-ViewModel) architectural pattern, which separates the application into three main components. This architecture helps to isolate the dependencies and makes the code more readable, reusable, and easier to test.

- **Networking**: The project includes a basic network layer for connecting to APIs. This layer provides a base for you to extend and customize according to your app's specific needs. You just have to change the base_url in info.plist.

- **Dependency Injection**: Dependency injection has been utilized to further ensure that each component of the application is as independent, interchangeable, and testable as possible.

- **Unit Tests**: The template includes examples of how to write unit tests for your ViewModels.

## Getting Started

To get started with this project, you'll need to have Xcode installed. Xcode can be downloaded from the Mac App Store.

1. **Clone the Repository**: Open your terminal and enter `git clone https://gitlab.com/NelsonP/startprojectswiftui.git`.

2. **Open the Project**: Navigate to the cloned repository's directory and double click on the `.xcodeproj` file or open it directly from the terminal using `open StarterProjectSwiftUI.xcodeproj`.

3. **Run the Application**: Choose the desired simulator and click on the Run button or press `CMD+R`.

## License

This project is licensed under the MIT License.

## Contact

If you have any questions, feel free to open an issue or submit a pull request. Happy coding!
