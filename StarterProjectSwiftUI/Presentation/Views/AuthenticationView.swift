//
//  AuthenticationView.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 28/06/2023.
//

import SwiftUI
import AuthenticationServices

struct AuthenticationView: View {
    @StateObject var viewModel = AuthenticationViewModel()
    
    @State private var username: String = ""
    @State private var password: String = ""
    
    var body: some View {
        VStack {
            TextField(Strings.common.username, text: $username)
                .padding()
                .background(Color(.systemGray6))
                .cornerRadius(8)
            
            SecureField(Strings.common.password, text: $password)
                .padding()
                .background(Color(.systemGray6))
                .cornerRadius(8)
            
            Button(action: {
                // Action to perform when button is pressed
                viewModel.signUp(loginId: username,
                                 password: password)
            }) {
                Text(Strings.authentication.createAccountAction)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundColor(.white)
                    .background(Color.blue)
                    .font(.system(size: 20))
                    .cornerRadius(10)
            }
            
            switch viewModel.signUpState {
            case .loading:
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .blue))
            case .loaded:
                Text("Account created successfully")
            case .error(let error):
                Text("Error: \(error.message)")
            case .idle:
                EmptyView()
            }
            
            // Login button
            Button(action: {
                // Action to perform when button is pressed
                viewModel.signIn(loginId: username,
                                 password: password)
            }) {
                Text(Strings.authentication.loginButtonAction)
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundColor(.white)
                    .background(Color.blue)
                    .font(.system(size: 20))
                    .cornerRadius(10)
            }
            
            switch viewModel.signInState {
            case .loading:
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .blue))
            case .loaded:
                Text("User logged successfully")
            case .error(let error):
                Text("Error: \(error.message)")
            case .idle:
                EmptyView()
            }
            
        }
        .padding(.horizontal, 16)
        
    }
    
}

struct AuthenticationView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationView()
    }
}
