//
//  AuthenticationViewModel.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 29/06/2023.
//

import Foundation
import SwiftUI

class AuthenticationViewModel: ObservableObject {
    
    @Injected(\.authenticationRepository) var authenticationRepository: AuthenticationRepositoryProtocol
    @Injected(\.userRepository) var userRepository: UserRepositoryProtocol
    @Injected(\.networkService) var networkService: NetworkServiceProtocol
    @Injected(\.keychainRepository) var keychainRepository: KeychainRepositoryProtocol

    @Published var signUpState: RequestState = .idle
    @Published var signInState: RequestState = .idle

    func signUp(loginId: String, password: String) {
        self.signUpState = .loading
        authenticationRepository.signUp(loginId: loginId, password: password) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let authentication):
                keychainRepository.saveToken(authentication.token)
                getUser()
                self.signUpState = .loaded
            case .failure(let error):
                self.signUpState = .error(error)
            }
        }
    }
    
    func signIn(loginId: String, password: String) {
        self.signInState = .loading
        authenticationRepository.signIn(loginId: loginId, password: password) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let authentication):
                keychainRepository.saveToken(authentication.token)
                getUser()
                self.signInState = .loaded
            case .failure(let error):
                self.signInState = .error(error)
            }
        }
    }
    
    func getUser() {
        userRepository.get() { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let user):
                print("user :\(user)")
                break
            case .failure(let error):
                self.signInState = .error(error)
            }
        }
    }
}
