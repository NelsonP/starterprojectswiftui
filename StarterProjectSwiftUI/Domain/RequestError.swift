//
//  ServerError.swift
//  mementop gait
//
//  Created by nelson parrilla on 17/07/2023.
//
import Foundation

enum RequestError: Error {
    case networkError(Error)
    case decodingError(Error)
    case parsingError(Error)
    case unknownError(Error)
    case invalidCredentials
    case itemNotFound
    case conflict
    case invalidToken
    
    enum StatusCode: Int {
        case invalidCredentials = 401
        case itemNotFound = 404
        case conflict = 409
        case invalidToken = 498
        
        func error(_ serverError: String) -> RequestError {
            switch self {
            case .invalidCredentials:
                return .invalidCredentials
            case .itemNotFound:
                return .itemNotFound
            case .conflict:
                return .conflict
            case .invalidToken:
                return .invalidToken
            }
        }
    }

    var message: String {
        switch self {
        case .networkError(let error),
             .decodingError(let error),
             .parsingError(let error):
            return error.localizedDescription
        case .unknownError(let error):
            return error.localizedDescription
        case .invalidCredentials:
            return Strings.errors.invalidToken
        case .itemNotFound:
            return Strings.errors.itemNotFound
        case .conflict:
            return Strings.errors.conflict
        case .invalidToken:
            return Strings.errors.invalidToken
        }
    }
}
