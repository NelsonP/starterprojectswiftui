//
//  Auth.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 11/07/2023.
//

import Foundation

struct Authentication: Decodable {
    let token: String
}
