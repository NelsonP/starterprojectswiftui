//
//  User.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 18/07/2023.
//

import Foundation

struct User: Decodable {
    let id: UUID
    let createdAt: Date
    let loginId: String
    let firstName: String?
    let password: String
    let email: String?

    enum CodingKeys: String, CodingKey {
        case id
        case createdAt
        case loginId
        case firstName
        case password
        case email
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(UUID.self, forKey: .id)
        let createdAtString = try container.decode(String.self, forKey: .createdAt)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" // Ajustez ce format à celui de votre date
        guard let date = dateFormatter.date(from: createdAtString) else {
            throw DecodingError.dataCorruptedError(forKey: .createdAt, in: container, debugDescription: "Date string does not match format expected by formatter.")
        }
        createdAt = date
        loginId = try container.decode(String.self, forKey: .loginId)
        firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        password = try container.decode(String.self, forKey: .password)
        email = try container.decodeIfPresent(String.self, forKey: .email)
    }
}
