//
//  Strings.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 20/07/2023.
//

import Foundation

enum Strings {
    
    private static var authenticationTableName: String = "Authentication"
    private static var commonTableName: String = "Common"
    private static var errorsTableName: String = "Errors"
    
    //MARK: - Authentication
    enum authentication {
        static let createAccountAction = NSLocalizedString("create.account.action", tableName: authenticationTableName, comment: "")
        static let loginButtonAction = NSLocalizedString("login.action", tableName: authenticationTableName, comment: "")
    }

    //MARK: - Common
    enum common {
        static let username = NSLocalizedString("username", tableName: commonTableName, comment: "")
        static let password = NSLocalizedString("password", tableName: commonTableName, comment: "")
    }
    
    //MARK: - Errors
    enum errors {
        static let invalidCredentials = NSLocalizedString("invalid.credentials.error", tableName: errorsTableName, comment: "")
        static let itemNotFound = NSLocalizedString("item.not.found.error", tableName: errorsTableName, comment: "")
        static let conflict = NSLocalizedString("conflict.error", tableName: errorsTableName, comment: "")
        static let invalidToken = NSLocalizedString("invalid.token.error", tableName: errorsTableName, comment: "")
    }
}
