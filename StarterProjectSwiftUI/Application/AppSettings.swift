//
//  AppSettings.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 10/07/2023.
//

import Foundation

enum InfoPlistKey: String {
    case base_url
}

struct AppSettings {
    
    private static var infoDict: [String: Any] {
        if let dict = Bundle.main.infoDictionary {
            return dict
        } else {
            fatalError("Info Plist file not found")
        }
    }
    
    static let base_url = infoDict[InfoPlistKey.base_url.rawValue] as! String
}
