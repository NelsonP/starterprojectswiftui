//
//  KeychainManager.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 18/07/2023.
//
import Foundation
import Security

protocol KeychainManagerProtocol {
    func saveValue(_ value: String, forKey key: String)
    func getValue(forKey key: String) -> String?
}

class KeychainManager: KeychainManagerProtocol {
    func saveValue(_ value: String, forKey key: String) {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: "com.StarterProjectSwiftUI.keychain",
            kSecAttrAccount as String: key,
            kSecValueData as String: value.data(using: .utf8)!,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked
        ]

        SecItemDelete(query as CFDictionary)
        SecItemAdd(query as CFDictionary, nil)
    }

    // Récupérer une valeur à partir d'une clé dans le Keychain
    func getValue(forKey key: String) -> String? {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: "com.StarterProjectSwiftUI.keychain",
            kSecAttrAccount as String: key,
            kSecReturnData as String: kCFBooleanTrue!,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]

        var dataTypeRef: AnyObject?
        let status = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

        if status == errSecSuccess,
           let retrievedData = dataTypeRef as? Data,
           let value = String(data: retrievedData, encoding: .utf8) {
            return value
        } else {
            return nil
        }
    }
}

class KeychainRepository: KeychainRepositoryProtocol {
    @Injected(\.keychainManager) var keychainManager: KeychainManagerProtocol

    func saveToken(_ token: String) {
        keychainManager.saveValue(token, forKey: "jwtToken")
    }

    func getToken() -> String? {
        return keychainManager.getValue(forKey: "jwtToken")
    }
}

protocol KeychainRepositoryProtocol {
    func saveToken(_ token: String)
    func getToken() -> String?
}

// MARK: Dependency injection management
private struct KeychainManagerProtocolKey: InjectionKey {
    static var currentValue: KeychainManagerProtocol = KeychainManager()
}

extension InjectedValues {
    var keychainManager: KeychainManagerProtocol {
        get {
            Self[KeychainManagerProtocolKey.self]
        }
        set {
            Self[KeychainManagerProtocolKey.self] = newValue
        }
    }
}

// MARK: Dependency injection management
private struct KeychainRepositoryProtocolKey: InjectionKey {
    static var currentValue: KeychainRepositoryProtocol = KeychainRepository()
}

extension InjectedValues {
    var keychainRepository: KeychainRepositoryProtocol {
        get {
            Self[KeychainRepositoryProtocolKey.self]
        }
        set {
            Self[KeychainRepositoryProtocolKey.self] = newValue
        }
    }
}
