//
//  Routes.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 13/07/2023.
//

import Foundation

struct Routes {
    static let base = "\(AppSettings.base_url)"
    static let signUp = "\(base)/authentication/signup"
    static let signIn = "\(base)/authentication/signin"
    static let getUser = "\(base)/user/"
}
