//
//  NetworkService.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 18/07/2023.
//

import Foundation

enum RequestState {
    case idle, loading, loaded, error(RequestError)
}

protocol NetworkServiceProtocol {
    func getRequest<T: Decodable>(url: URL, completion: @escaping (Result<T, RequestError>) -> Void)
    func postRequest<T: Decodable>(url: URL, jsonData: Data, completion: @escaping (Result<T, RequestError>) -> Void)
}

class NetworkService: NetworkServiceProtocol {
    @Injected(\.keychainRepository) var keychainRepository: KeychainRepositoryProtocol

    enum HTTPMethod: String {
        case GET, POST
    }
    
    func getRequest<T: Decodable>(url: URL, completion: @escaping (Result<T, RequestError>) -> Void) {
        request(.GET, url: url, completion: completion)
    }

    func postRequest<T: Decodable>(url: URL, jsonData: Data, completion: @escaping (Result<T, RequestError>) -> Void) {
        request(.POST, url: url, jsonData: jsonData, completion: completion)
    }
    
    private func request<T: Decodable>(_ method: HTTPMethod, url: URL, jsonData: Data? = nil, completion: @escaping (Result<T, RequestError>) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.setValue(keychainRepository.getToken(), forHTTPHeaderField: "Authorization")
        if method == .POST {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
        }

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("Network Error:", error)
                completion(.failure(.networkError(error)))
            } else if let data = data, let httpResponse = response as? HTTPURLResponse {
                if (200...299).contains(httpResponse.statusCode) {
                    do {
                        let decoder = JSONDecoder()
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        let responseObject = try decoder.decode(T.self, from: data)
                        completion(.success(responseObject))
                    } catch {
                        print("Decoding Error:", error)
                        completion(.failure(.decodingError(error)))
                    }
                } else {
                    // Handle error response here
                    do {
                        if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                           let serverError = jsonResponse["error"] as? String,
                           let statusCode = RequestError.StatusCode(rawValue: httpResponse.statusCode) {
                            print("API Error:", serverError)
                            completion(.failure(statusCode.error(serverError)))
                        } else {
                            let error = NSError(domain: "", code: httpResponse.statusCode, userInfo: nil)
                            print("Unknown Error:", error)
                            completion(.failure(.unknownError(error)))
                        }
                    } catch {
                        print("Parsing Error:", error)
                        completion(.failure(.parsingError(error)))
                    }
                }
            }
        }

        task.resume()
    }
}

// MARK: Dependency injection management
private struct NetworkServiceProtocolKey: InjectionKey {
    static var currentValue: NetworkServiceProtocol = NetworkService()
}

extension InjectedValues {
    var networkService: NetworkServiceProtocol {
        get {
            Self[NetworkServiceProtocolKey.self]
        }
        set {
            Self[NetworkServiceProtocolKey.self] = newValue
        }
    }
}
