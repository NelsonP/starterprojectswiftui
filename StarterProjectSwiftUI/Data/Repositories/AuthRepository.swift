//
//  AuthenticationRepository.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 11/07/2023.
//

import Foundation


protocol AuthenticationRepositoryProtocol {
    /// Method for creating a new user account.
    ///
    /// - Parameters:
    ///     - loginId: User identifier.
    ///     - password: User password.
    ///     - completion: Completion closure to handle the server response.
    func signUp(loginId: String, password: String, completion: @escaping (Result<Authentication, RequestError>) -> Void)
    
    /// Method for an existing user to log into their account.
    ///
    /// - Parameters:
    ///     - loginId: User identifier.
    ///     - password: User password.
    ///     - completion: Completion closure to handle the server response.
    func signIn(loginId: String, password: String, completion: @escaping (Result<Authentication, RequestError>) -> Void)
}

class AuthenticationRepository: ObservableObject, AuthenticationRepositoryProtocol {
    @Injected(\.networkService) var networkService: NetworkServiceProtocol

    func signUp(loginId: String, password: String, completion: @escaping (Result<Authentication, RequestError>) -> Void) {
        let json: [String: Any] = ["loginId": loginId, "password": password]
        guard let serverSignupURL = URL(string: Routes.signUp) else {
            fatalError("wrong URL")
        }

        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
            networkService.postRequest(url: serverSignupURL, jsonData: jsonData, completion: completion)
        } catch {
            completion(.failure(.unknownError(error)))
        }
    }
    
    func signIn(loginId: String, password: String, completion: @escaping (Result<Authentication, RequestError>) -> Void) {
        let json: [String: Any] = ["loginId": loginId, "password": password]
        guard let serverSigninURL = URL(string: Routes.signIn) else {
            fatalError("wrong URL")
        }

        do {
            let jsonData = try JSONSerialization.data(withJSONObject: json, options: [])
            networkService.postRequest(url: serverSigninURL, jsonData: jsonData, completion: completion)
        } catch {
            completion(.failure(.unknownError(error)))
        }
    }
}

// MARK: Dependency injection management
private struct AuthenticationRepositoryProtocolKey: InjectionKey {
    static var currentValue: AuthenticationRepositoryProtocol = AuthenticationRepository()
}

extension InjectedValues {
    var authenticationRepository: AuthenticationRepositoryProtocol {
        get {
            Self[AuthenticationRepositoryProtocolKey.self]
        }
        set {
            Self[AuthenticationRepositoryProtocolKey.self] = newValue
        }
    }
}
