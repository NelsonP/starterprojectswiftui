//
//  AuthenticationRepository.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 11/07/2023.
//

import Foundation


protocol UserRepositoryProtocol {
    /// Get user.
    ///
    /// - Parameters:
    ///     - completion: Completion closure to handle the server response.
    func get(completion: @escaping (Result<User, RequestError>) -> Void)
}

class UserRepository: ObservableObject, UserRepositoryProtocol {
    @Injected(\.networkService) var networkService: NetworkServiceProtocol

    func get(completion: @escaping (Result<User, RequestError>) -> Void) {

        guard let serverSignupURL = URL(string: Routes.getUser) else {
            fatalError("wrong URL")
        }

        networkService.getRequest(url: serverSignupURL, completion: completion)
    }
}

// MARK: Dependency injection management
private struct UserRepositoryProtocolKey: InjectionKey {
    static var currentValue: UserRepositoryProtocol = UserRepository()
}

extension InjectedValues {
    var userRepository: UserRepositoryProtocol {
        get {
            Self[UserRepositoryProtocolKey.self]
        }
        set {
            Self[UserRepositoryProtocolKey.self] = newValue
        }
    }
}
