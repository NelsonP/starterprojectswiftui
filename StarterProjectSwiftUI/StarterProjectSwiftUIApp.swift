//
//  StarterProjectSwiftUIApp.swift
//  StarterProjectSwiftUI
//
//  Created by nelson parrilla on 12/07/2023.
//

import SwiftUI

@main
struct StarterProjectSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            AuthenticationView()
        }
    }
}
