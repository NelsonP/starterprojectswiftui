//
//  AuthenticationViewModelTests.swift
//  StarterProjectSwiftUITests
//
//  Created by nelson parrilla on 20/07/2023.
//

import XCTest
@testable import StarterProjectSwiftUI

class AuthenticationViewModelTests: XCTestCase {
    
    var viewModel = AuthenticationViewModel()

    override func setUp() {
        super.setUp()
        InjectedValues[\.authenticationRepository] = MockAuthenticationRepository()
    }

    func testSignUp() {
        viewModel.signUp(loginId: "test", password: "test")
        
        if case .loaded = viewModel.signUpState {
            XCTAssertTrue(true)
        } else {
            XCTFail("Sign Up state should be .loaded")
        }
    }
    
    func testSignIn() {
        viewModel.signIn(loginId: "test", password: "test")
        if case .loaded = viewModel.signInState {
            XCTAssertTrue(true)
        } else {
            XCTFail("Sign In state should be .loaded")
        }
    }
}

