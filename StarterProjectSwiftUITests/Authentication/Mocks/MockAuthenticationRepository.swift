//
//  MockAuthenticationRepository.swift
//  StarterProjectSwiftUITests
//
//  Created by nelson parrilla on 12/07/2023.
//

import Foundation
@testable import StarterProjectSwiftUI

class MockAuthenticationRepository: AuthenticationRepositoryProtocol {
    func signUp(loginId: String, password: String, completion: @escaping (Result<Authentication, RequestError>) -> Void) {
        completion(.success(Authentication(token: "authToken")))
    }
    
    func signIn(loginId: String, password: String, completion: @escaping (Result<Authentication, RequestError>) -> Void) {
        completion(.success(Authentication(token: "authToken")))
    }
}
